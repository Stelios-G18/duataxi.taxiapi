﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DuaTaxi.Services.TaxiApi.Entities.DTO
{
    public class SetPickUpDto
    {
        public string Id { get; set; }

        public double Lat { get; set; }

        public double Long { get; set; }

        public string AddressFrom { get; set; }

        public string AddressTo { get; set; }

        public List<MarkerDto> Marker { get; set; }
    }
}
