﻿using DuaTaxi.Common.Types;
using DuaTaxi.Services.TaxiApi.Entities.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DuaTaxi.Services.TaxiApi.Entities.Models
{
    public class TransportHistory : BaseEntity
    {            
        public string From { get; set; }
        public string To { get; set; }
        public string DriverName { get; set; }
        public string DriverId { get; set; }
        public DriverPositionStatus DriverPositionStatus { get; set; }
        public List<MarkerDto> Marker { get; set; }

        public TransportHistory()
        {

        }
    }
}
