﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DuaTaxi.Common.Authentication;
using DuaTaxi.Common.Types;
using DuaTaxi.Common.WebApiClient;
using DuaTaxi.Repositories.CustomerRepo;
using DuaTaxi.Services.TaxiApi.Entities.DTO;
using DuaTaxi.Services.TaxiApi.Framework;
using DuaTaxi.Services.TaxiApi.Services.CarDetails;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Net.Http.Headers;

namespace DuaTaxi.Services.TaxiApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [ServiceFilter(typeof(AuthValidation))]
    public class CarDetailsController : ControllerBase
    {
        private ILogger<CarDetailsController> _logger;
        ICarDetailsService _carDetailsService;
        public CarDetailsController(ICarDetailsService carDetailsService, ILogger<CarDetailsController> logger)
        {
            _carDetailsService = carDetailsService;
            _logger = logger;
        }


        [Route("GetById/{customerID}")]
        [HttpGet]
        public async Task<SrvResp<CarLicenseDto>> Get(string customerId)
        {                
            try {
                var Customer = await _carDetailsService.GetCarDetails(customerId);

                if (Customer is null)
                    return new SrvResp<CarLicenseDto>();

                return  new SrvResp<CarLicenseDto>(Customer);


            } catch (DuaTaxiException ex) {
                _logger.LogError(ex.Message, ex.InnerException, ex.StackTrace);
                return new SrvResp<CarLicenseDto>(ex);
            }

        }

        [Route("Update/")]
        [HttpPost]
        public async Task<SrvResp<CarLicenseDto>> Put([FromBody] CarLicenseDto carLicense)
        {
            try {
                var updateDetails = await _carDetailsService.UpdateCarDetails(carLicense);
                if (updateDetails) return new SrvResp<CarLicenseDto>(carLicense);

                return new SrvResp<CarLicenseDto>();


            } catch (DuaTaxiException ex) {
                _logger.LogError(ex.Message, ex.InnerException, ex.StackTrace);
                return new SrvResp<CarLicenseDto>(ex);
            }

        }
    }
}