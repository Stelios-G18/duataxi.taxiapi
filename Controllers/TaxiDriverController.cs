﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DuaTaxi.Common.Authentication;
using DuaTaxi.Common.CustomCheck;
using DuaTaxi.Common.Dispatchers;
using DuaTaxi.Common.Mvc;
using DuaTaxi.Common.Types;
using DuaTaxi.Common.WebApiClient;
using DuaTaxi.Service.TaxiApi.Dto;
using DuaTaxi.Service.TaxiApi.Query;
using DuaTaxi.Services.TaxiApi.Entities.DTO;
using DuaTaxi.Services.TaxiApi.Framework;
using DuaTaxi.Services.TaxiApi.Services.SetPickUp;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Net.Http.Headers;

namespace DuaTaxi.Service.TaxiApi.Controllers {

    [Route("api/[controller]")]
    [ApiController]
    [ServiceFilter(typeof(AuthValidation))]
    public class TaxiDriverController : ControllerBase {
      
        private readonly IDispatcher _dispatcher;
        private ILogger<TaxiDriverController> _logger;
        private readonly ISetPickUpService _setPickUpHandler;
        CheckOptions _check;




        public TaxiDriverController(IDispatcher dispatcher,  ILogger<TaxiDriverController> logger, ISetPickUpService setPickUpHandler, CheckOptions check) {
            _dispatcher = dispatcher;                 
            
            _logger = logger;
            _setPickUpHandler = setPickUpHandler;
            _check = check;

        }

        // Idempotent
        // No side effects
        // Doesn't mutate a state
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CreateTaxiDriverDto>>> Get([FromQuery] BrowseTaxiDrive query)
            => Ok(await _dispatcher.QueryAsync(query));

        //[HttpGet("{id:guid}")]
        //public async Task<ActionResult<DiscountDetailsDto>> Get([FromRoute] GetDiscount query)
        //{
        //    var discount = await _dispatcher.QueryAsync(query);
        //    if (discount is null)
        //    {
        //        return NotFound();
        //    }

        //    return discount;
        //}

        //[HttpPost]
        //public async Task<ActionResult> Post(CreateTaxiDriver command) {
        //    await _dispatcher.SendAsync(command.BindId(c => c.Id));

        //    return Accepted();
        //}


     
        [Route("SetPickup")]
        [HttpPost]               
        public async Task<SrvResp<ClosesActiveDriversDTO>> SetPickup([FromBody] SetPickUpDto setPickDto)
        {
            try {
                //var token = Request.Headers[HeaderNames.Authorization];
                //if(token != _check.Password) {
                //    HttpContext.Response.StatusCode = 401;
                //    return null; 
                //}
               
                var driverData = await _setPickUpHandler.HandleAsync(setPickDto);

                if (driverData is null)
                    return new SrvResp<ClosesActiveDriversDTO>();

                return new SrvResp<ClosesActiveDriversDTO>(driverData);

            } catch (DuaTaxiException ex) {
                _logger.LogError(ex.Message, ex.InnerException, ex.StackTrace);
                return new SrvResp<ClosesActiveDriversDTO>(ex);
            }                                                 
        }



    }
}