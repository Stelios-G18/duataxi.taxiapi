﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DShop.Common.Authentication;
using DuaTaxi.Common.Authentication;
using DuaTaxi.Common.Types;
using DuaTaxi.Common.WebApiClient;
using DuaTaxi.Repositories.TaxiDriverStatusRepo;
using DuaTaxi.Service.TaxiApi.Entities;
using DuaTaxi.Services.TaxiApi.Framework;
using DuaTaxi.Services.TaxiApi.Services.InitializeTaxiDriver;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace DuaTaxi.Services.TaxiApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [ServiceFilter(typeof(AuthValidation))]

    public class InitDriverController : ControllerBase
    {
        private readonly ITaxiDriverStatusRepository _taxiDriverStatusRepository;
        private ILogger<InitDriverController> _logger;
        private readonly IInitializeTaxiDriverService _initializeTaxiDriver;
        public InitDriverController(ILogger<InitDriverController> logger, IInitializeTaxiDriverService initializeTaxiDriver, ITaxiDriverStatusRepository taxiDriverStatusRepository)
        {
            _initializeTaxiDriver = initializeTaxiDriver;
            _logger = logger;
            _taxiDriverStatusRepository = taxiDriverStatusRepository;

        }


        [Route("GetById/{customerID}")]
        [HttpGet]       
        public async Task<SrvResp<TaxiDriverStatus>> TaxiDriverStatus(string customerId)
        {
            try {
                var TaxiDriverStatus = await _taxiDriverStatusRepository.GetCustomerAsync(customerId);

                if (TaxiDriverStatus is null) {
                    TaxiDriverStatus = await _initializeTaxiDriver.HandleAsync(customerId);
                }

                return new SrvResp<TaxiDriverStatus>(TaxiDriverStatus);


            } catch (DuaTaxiException ex) {
                _logger.LogError(ex.Message, ex.InnerException, ex.StackTrace);
                return new SrvResp<TaxiDriverStatus>(ex);
            }
        }
    }
}