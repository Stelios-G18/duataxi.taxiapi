﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DuaTaxi.Common.Authentication;
using DuaTaxi.Common.Types;
using DuaTaxi.Common.WebApiClient;
using DuaTaxi.Repositories.CustomerRepo;
using DuaTaxi.Services.TaxiApi.Entities.Models;
using DuaTaxi.Services.TaxiApi.Framework;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Net.Http.Headers;

namespace DuaTaxi.Services.TaxiApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [ServiceFilter(typeof(AuthValidation))]
    public class CustomerController : ControllerBase
    {
        private ILogger<CustomerController> _logger;
        private readonly ICustomerRepository _customerRepository;
        public CustomerController(ICustomerRepository customerRepository, ILogger<CustomerController> logger)
        {
            _customerRepository = customerRepository;
            _logger = logger;
        }


        [Route("GetById/{customerID}")]
        [HttpGet]
        public async Task<SrvResp<Customer>> Get(string customerId)
        {                
            try {
                var Customer = await _customerRepository.GetAsync(customerId);

                if (Customer is null)
                    return null;

                return new SrvResp<Customer>(Customer);


            } catch (DuaTaxiException ex) {
                _logger.LogError(ex.Message, ex.InnerException, ex.StackTrace);
                return new SrvResp<Customer>(ex);
            }
        }
    }

}