﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DuaTaxi.Common.Authentication;
using DuaTaxi.Common.CustomCheck;
using DuaTaxi.Common.RabbitMq;
using DuaTaxi.Common.Types;
using DuaTaxi.Common.WebApiClient;
using DuaTaxi.Services.TaxiApi.Entities.Models;
using DuaTaxi.Services.TaxiApi.Services.TransportHistoryService;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using OpenTracing;

namespace DuaTaxi.Services.TaxiApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [ServiceFilter(typeof(AuthValidation))]
    public class RideController : ControllerBase
    {
        private ILogger<RideController> _logger;
        private ITransportHistoryService _transportHistoryService;
        public RideController(ILogger<RideController> logger ,ITransportHistoryService transportHistoryService)
        {
            this._logger = logger;
            this._transportHistoryService = transportHistoryService;
        }

        [Route("GetRide/{driverID}")]
        [HttpGet]
        public async Task<SrvResp<TransportHistory>> GetRide(string driverID)
        {
            try {

                var data = await _transportHistoryService.Get(driverID);
                if (data is null) {
                    return new SrvResp<TransportHistory>();
                }

                return new SrvResp<TransportHistory>(data);


            } catch (DuaTaxiException ex) {
                _logger.LogError(ex.Message, ex.InnerException, ex.StackTrace);
                return new SrvResp<TransportHistory>(ex);
            }

        }

        [Route("UpdateRide/")]
        [HttpPost]
        public async Task<SrvResp<TransportHistory>> UpdateRide([FromBody] TransportHistory history)
        {
            try {
                var data = await _transportHistoryService.Update(history);
                
                    return new SrvResp<TransportHistory>();

            } catch (DuaTaxiException ex) {
                _logger.LogError(ex.Message, ex.InnerException, ex.StackTrace);
                return new SrvResp<TransportHistory>(ex);
            }

        }
        [Route("CreateNewRide/")]
        [HttpPost]
        public async Task<SrvResp<TransportHistory>> CreateNewRide([FromBody] TransportHistory history)
        {
            try {
                var data = await _transportHistoryService.Create(history);
                
                    return new SrvResp<TransportHistory>();

            } catch (DuaTaxiException ex) {
                _logger.LogError(ex.Message, ex.InnerException, ex.StackTrace);
                return new SrvResp<TransportHistory>(ex);
            }

        }
    }
}