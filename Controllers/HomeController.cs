﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DuaTaxi.Common.Authentication;
using Microsoft.AspNetCore.Mvc;

namespace DuaTaxi.Service.TaxiApi.Controllers
{
    [Route("[controller]")]   
    public class HomeController : ControllerBase
    {
        [HttpGet]
        [ServiceFilter(typeof(AuthValidation))]
        public IActionResult Get() => Ok("DuaTaxi TaxiApi Service");
    }
}