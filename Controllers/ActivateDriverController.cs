﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DuaTaxi.Common.Authentication;
using DuaTaxi.Common.Types;
using DuaTaxi.Common.WebApiClient;
using DuaTaxi.Repositories.ActiveDriversRepo;
using DuaTaxi.Services.TaxiApi.Entities.Models;
using DuaTaxi.Services.TaxiApi.Framework;
using DuaTaxi.Services.TaxiApi.Messages.Commands;

using DuaTaxi.Services.TaxiApi.Services.DriverActivation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace DuaTaxi.Services.TaxiApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [ServiceFilter(typeof(AuthValidation))]
    public class ActivateDriverController : ControllerBase
    {
        private readonly IActiveTaxiDriverRepository _activeTaxiDriverRepository;
        private readonly ITaxiDriverActivationService _taxiDriverActivation;
        private ILogger<ActivateDriverController> _logger;
        public ActivateDriverController(ILogger<ActivateDriverController> logger, ITaxiDriverActivationService taxiDriverActivation, IActiveTaxiDriverRepository activeTaxiDriverRepository)
        {
            _logger = logger;
            _activeTaxiDriverRepository = activeTaxiDriverRepository;
            _taxiDriverActivation = taxiDriverActivation;
        }


        [Route("DeActivate/{customerID}")]
        [HttpGet]
        public async Task<SrvResp<ActiveDrivers>> DeActivateDriver(string customerId)
        {
            try {
                var deactivate = await _activeTaxiDriverRepository.DeActivate(customerId);

                return new SrvResp<ActiveDrivers>(deactivate);
            } catch (DuaTaxiException ex) {
                _logger.LogError(ex.Message, ex.InnerException, ex.StackTrace);
                return new SrvResp<ActiveDrivers>(ex);
            }
        }


        [Route("ReActivate/{customerID}")]
        [HttpGet]
        public async Task<SrvResp<bool>> ReActivateDriver(string customerId)
        {
            try {
                var deactivate = await _activeTaxiDriverRepository.ReActivate(customerId);

                if (deactivate.IsActive)
                    return new SrvResp<bool>(true);

                return new SrvResp<bool>(false);
            } catch (DuaTaxiException ex) {
                _logger.LogError(ex.Message, ex.InnerException, ex.StackTrace);
                return new SrvResp<bool>(ex);
            }
        }



        [Route("Activation")]
        [HttpPost]
        public async Task<SrvResp<ActiveDrivers>> TaxiDriverActivation([FromBody] ActiveDriverDTO taxiDriverActivation)
        {
            try {

                var driverData = await _taxiDriverActivation.HandleAsync(taxiDriverActivation);

                if (driverData is null)
                    return new SrvResp<ActiveDrivers>();

                return new SrvResp<ActiveDrivers>(driverData);

            } catch (DuaTaxiException ex) {
                _logger.LogError(ex.Message, ex.InnerException, ex.StackTrace);
                return new SrvResp<ActiveDrivers>(ex);
            }
        }

    }
}