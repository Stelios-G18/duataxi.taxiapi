﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DuaTaxi.Services.TaxiApi.Entities.Models;

namespace DuaTaxi.Services.TaxiApi.Repositories.TransportHistoryRepo
{
    public interface ITransportHistoryRepository
    {
        Task AddAsync(TransportHistory history);
        Task UpdateAsync(TransportHistory history);

        Task DeleteAsync(TransportHistory history);

        Task<TransportHistory> GetAsync(string Id);

        Task<IEnumerable<TransportHistory>> GetDriverByIdAsync(string Id);
    }
}
