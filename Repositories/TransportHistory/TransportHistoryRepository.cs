﻿using DuaTaxi.Common.Mongo;
using DuaTaxi.Common.Types;
using DuaTaxi.Services.TaxiApi.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DuaTaxi.Services.TaxiApi.Repositories.TransportHistoryRepo
{
    public class TransportHistoryRepository   : ITransportHistoryRepository
    {
        IMongoRepository<TransportHistory> _repository;
        public TransportHistoryRepository(IMongoRepository<TransportHistory> repository)
        {
            _repository = repository;
        }
        public async Task AddAsync(TransportHistory history)
            => await _repository.AddAsync(history);

        public async Task DeleteAsync(TransportHistory history)
            => await _repository.DeleteAsync(history.Id);

        public async Task<TransportHistory> GetAsync(string Id)
            => await _repository.GetAsync(Id);

        public async Task<IEnumerable<TransportHistory>> GetDriverByIdAsync(string Id)
            => await _repository.FindAsync(c => c.DriverId == Id && c.DriverPositionStatus != DriverPositionStatus.RideFinished);

        public async Task UpdateAsync(TransportHistory history)
            => await _repository.UpdateAsync(history);
    }
}
