﻿using DuaTaxi.Common.Handlers;
using DuaTaxi.Common.RabbitMq;
using DuaTaxi.Repositories.ActiveDriversRepo;
using DuaTaxi.Repositories.DriverMapPositionRepo;
using DuaTaxi.Repositories.TaxiDriverStatusRepo;
using DuaTaxi.Services.TaxiApi.Entities.Models;
using DuaTaxi.Services.TaxiApi.Messages.Commands;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DuaTaxi.Services.TaxiApi.Handlers.TaxiDrivers
{
    public class DriverMapPositionHandler : ICommandHandler<TaxiDriverMapPosition>
    {
        private readonly ITaxiDriverStatusRepository _taxiDriverRepository;
        private readonly IActiveTaxiDriverRepository _activeTaxiDriverRepository;
        private readonly IDriverMapPositionService _driverMapPositionService;
        private readonly ILogger<DriverMapPositionHandler> _logger;

        public DriverMapPositionHandler(ITaxiDriverStatusRepository taxiDriverRepository, IActiveTaxiDriverRepository activeTaxiDriverRepository, IDriverMapPositionService driverMapPositionService, ILogger<DriverMapPositionHandler> logger)
        {
            _taxiDriverRepository = taxiDriverRepository;
            _activeTaxiDriverRepository = activeTaxiDriverRepository;
            _driverMapPositionService = driverMapPositionService;
            _logger = logger;
        }


        public async Task HandleAsync(TaxiDriverMapPosition command, ICorrelationContext context)
        {
            var driver = await _activeTaxiDriverRepository.GetCustomerAsync(command.CustomerId);
            //if (!driver.IsActive) {
            //    _logger.LogError($"Driver with id: {command.CustomerId}  not exist", "DriverMapPositionHandler---> set Driver Map Position");
            //    return;
            //}
            var findDriver =await _activeTaxiDriverRepository.GetCustomerAsync(command.CustomerId);

            await _activeTaxiDriverRepository.UpdateAsync(new ActiveDrivers(findDriver.Id, command.CustomerId, command.Latitude, command.Longtitute, findDriver.IsActive, command.ConnectionId));

          //  await _driverMapPositionService.UpdateAsync(new DriversMapPosition(command.Id, command.CustomerId, command.Latitude, command.Longtitute));

            return;
        }
    }
}
