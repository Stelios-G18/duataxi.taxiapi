﻿using DuaTaxi.Service.TaxiApi.Dto;
using DuaTaxi.Services.TaxiApi.Entities.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DuaTaxi.Services.TaxiApi.Services.CarDetails
{
    public interface ICarDetailsService
    {
        Task<CarLicenseDto> GetCarDetails(string customerId);

        Task<bool> UpdateCarDetails(CarLicenseDto carLicense);
    }
}
