﻿using DuaTaxi.Repositories.CustomerRepo;
using DuaTaxi.Service.TaxiApi.Dto;
using DuaTaxi.Services.TaxiApi.Entities.DTO;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DuaTaxi.Services.TaxiApi.Services.CarDetails
{
    public class CarDetailsService : ICarDetailsService
    {
        private readonly ILogger<CarDetailsService> _logger;
        private readonly ICustomerRepository _customerRepo;

        public CarDetailsService(ILogger<CarDetailsService> logger, ICustomerRepository customerRepo)
        {
            _logger = logger;
            _customerRepo = customerRepo;

        }

        public async Task<CarLicenseDto> GetCarDetails(string customerId)
        {
            var cardetails = await _customerRepo.GetAsync(customerId);

            if (String.IsNullOrEmpty(cardetails.CarPlate)) {
                var details = new CarLicenseDto() {
                    Id = customerId,
                    IsUpdate = false,
                    LicenseNumber = "",
                };
                return details;
            }
            else {

                var details = new CarLicenseDto() {
                    Id = customerId,
                    IsUpdate = true,
                    LicenseNumber = cardetails.CarPlate,
                };
                return details;
            }
        }


        public async Task<bool> UpdateCarDetails(CarLicenseDto carLicense)
        {
            try {

                var cardetails = await _customerRepo.GetAsync(carLicense.Id);
                cardetails.CarPlate = carLicense.LicenseNumber;

                await _customerRepo.UpdateAsync(cardetails);

                return true;


            } catch (Exception ex) {
                _logger.LogError("Update CarLicense fail", ex.Message, ex.StackTrace);
                return false;
            }


        }
    }
}
