﻿using DuaTaxi.Common.RestEase;
using DuaTaxi.Common.WebApiClient;
using DuaTaxi.Services.TaxiApi.Services.InitializeTaxiDriver;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace DuaTaxi.Services.TaxiApi.Services.TimeHostService
{
    public class TimedHostedService : IHostedService, IDisposable
    {
        private int executionCount = 0;
        private readonly ILogger<TimedHostedService> _logger;
        private Timer _timer;
        private readonly IInitializeTaxiDriverService _initializeTaxiDriverService;
        
        public TimedHostedService(ILogger<TimedHostedService> logger, IInitializeTaxiDriverService initializeTaxiDriverService)
        {
            _logger = logger;
            _initializeTaxiDriverService = initializeTaxiDriverService;
            
        }

        public Task StartAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation("Timed Hosted Service running.");


            _timer = new Timer(DoWork, null, TimeSpan.Zero,
                TimeSpan.FromHours(1));

            return Task.CompletedTask;
        }

        private void DoWork(object state)
        {
            try {                 
                _initializeTaxiDriverService.DeleteAllTAxiDriverStatusAsync();

                var count = Interlocked.Increment(ref executionCount);
                _logger.LogInformation(
                    "Timed Hosted Service is working. DeleteAllTAxiDriverStatusAsync : {Count}", count);
            } catch (Exception) {

                throw;
            }




        }

        public Task StopAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation("Timed Hosted Service is stopping.");

            _timer?.Change(Timeout.Infinite, 0);

            return Task.CompletedTask;
        }

        public void Dispose()
        {
            _timer?.Dispose();
        }
    }
}
