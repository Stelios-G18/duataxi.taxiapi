﻿using DuaTaxi.Services.TaxiApi.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DuaTaxi.Services.TaxiApi.Services.TransportHistoryService
{
    public interface ITransportHistoryService
    {

        Task<bool> Create(TransportHistory history);
        Task<bool> Update(TransportHistory history);
        Task<TransportHistory> Get(string driverID);
    }
}
