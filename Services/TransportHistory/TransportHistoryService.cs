﻿using DuaTaxi.Services.TaxiApi.Entities.Models;
using DuaTaxi.Services.TaxiApi.Repositories.TransportHistoryRepo;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DuaTaxi.Services.TaxiApi.Services.TransportHistoryService
{
    public class TransportHistoryService: ITransportHistoryService
    {
        private readonly ITransportHistoryRepository _transportHistoryRepository;
        private readonly ILogger<TransportHistoryService> _logger;
        public TransportHistoryService(ITransportHistoryRepository transportHistoryRepository , ILogger<TransportHistoryService> logger)
        {
            this._transportHistoryRepository = transportHistoryRepository;
            this._logger = logger;
        }

        public async Task<TransportHistory> Get(string driverID)
        {
            try {
                var historyDatabyDriverId = await _transportHistoryRepository.GetDriverByIdAsync(driverID);
                return historyDatabyDriverId.FirstOrDefault();
            } catch (Exception ex) {

                _logger.LogError("TransportHistoryService Get error ----->  :", ex.Message);
                throw new Exception();
            }

        }


        public async Task<bool> Create(TransportHistory history)
        {
            try {
                await _transportHistoryRepository.AddAsync(history);
                return true;
            } catch (Exception ex) {

                _logger.LogError("TransportHistoryService Create error ----->  :", ex.Message);
                throw new Exception();
            }
          
        }

        public async Task<bool> Update(TransportHistory history)
        {
            try {
                await _transportHistoryRepository.UpdateAsync(history);
                return true;
            } catch (Exception ex) {

                _logger.LogError("TransportHistoryService Update error ----->  :", ex.Message);
                throw new Exception();
            }
        }

      
    }
}
