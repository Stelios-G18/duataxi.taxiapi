﻿using DuaTaxi.Common;
using DuaTaxi.Repositories.ActiveDriversRepo;
using DuaTaxi.Repositories.CustomerRepo;
using DuaTaxi.Services.TaxiApi.Entities.DTO;
using DuaTaxi.Services.TaxiApi.Entities.Models;
using DuaTaxi.Services.TaxiApi.Hubs.Services;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DuaTaxi.Services.TaxiApi.Services.SetPickUp
{
    public class SetPickUpService : ISetPickUpService
    {
        private readonly IActiveTaxiDriverRepository _activeTaxiDriverRepository;
        private readonly ILogger<SetPickUpService> _logger;
        private readonly ICustomerRepository _customerRepository;
        private readonly IHubWrapper _hubWrapper;
        private List<ClosesActiveDriversDTO> Distance;
        ClosesActiveDriversDTO driver;

        public SetPickUpService(IActiveTaxiDriverRepository activeTaxiDriverRepository, ICustomerRepository customerRepository, ILogger<SetPickUpService> logger, IHubWrapper hubWrapper)
        {
            _activeTaxiDriverRepository = activeTaxiDriverRepository;
            _logger = logger;
            _hubWrapper = hubWrapper;
            _customerRepository = customerRepository;
        }
        public async Task<ClosesActiveDriversDTO> HandleAsync(SetPickUpDto setPick)
        {
            Distance = new List<ClosesActiveDriversDTO>();
            
            var allActiveDrive = await _activeTaxiDriverRepository.GetAllActiveDriverAsync();
            Distance = await FindClosesDrivers(allActiveDrive, Distance, setPick);

            foreach (var item in Distance) {
                var response = await _hubWrapper.PublishToUserConnectionIDAsync(item.ConnectionId, "", setPick);
                if(response == null || response == "False") {
                    continue;
                }
                var deactivate = await _activeTaxiDriverRepository.DeActivate(item.Id);
                if( deactivate is null) {
                    continue;
                }
                return item;            
            }      
           
            var DriverNotFound = new ClosesActiveDriversDTO {
                Id = "",
                Distance = 0,
                Error = "Driver Not Found",
                ConnectionId = "",
            };                   

            return DriverNotFound;
        }

        public async Task<List<ClosesActiveDriversDTO>> FindClosesDrivers(IEnumerable<ActiveDrivers> allActiveDrive, List<ClosesActiveDriversDTO> Distance, SetPickUpDto setPick)
        {
            int maxNumberOfDrivers = 5;

            foreach (var item in allActiveDrive) {

                var customer = await _customerRepository.GetAsync(item.CustomerId);
                driver = new ClosesActiveDriversDTO {
                    Id = item.CustomerId,
                    ConnectionId = item.ConnectionId,
                    Distance = new Coordinates(setPick.Marker[0].Latitude, setPick.Marker[0].Longitude)
                                   .DistanceTo(
                                        new Coordinates(item.Latitude, item.Longtitute),
                                        UnitOfLength.Kilometers) ,
                    Error="Null",
                    DriverName =customer.Name,
                    DriverPhoneNumber= customer.PhoneNumber,
                    Lat = item.Latitude,
                    Long= item.Longtitute,
                    CarPlate = customer.CarPlate

                };

                if(driver.Distance > 25.000) {
                    continue;
                }

                if (Distance.Count <= maxNumberOfDrivers) {
                    Distance.Add(driver);
                    Distance = Distance.OrderBy(d => d.Distance).ToList();
                }
                else {
                    if (driver.Distance > Distance[maxNumberOfDrivers].Distance) {
                        continue;
                    }
                    else {
                        Distance[maxNumberOfDrivers] = driver;
                    }
                    Distance = Distance.OrderBy(d => d.Distance).ToList();
                }
            }
            return Distance;

        }

       

    }
}
