﻿using DuaTaxi.Services.TaxiApi.Entities.DTO;
using DuaTaxi.Services.TaxiApi.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DuaTaxi.Services.TaxiApi.Services.SetPickUp
{
   public interface ISetPickUpService
    {
        Task<ClosesActiveDriversDTO> HandleAsync(SetPickUpDto setPick);
    }
}
