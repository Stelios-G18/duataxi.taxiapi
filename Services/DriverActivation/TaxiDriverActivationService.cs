﻿using DuaTaxi.Repositories.ActiveDriversRepo;
using DuaTaxi.Repositories.TaxiDriverStatusRepo;
using DuaTaxi.Services.TaxiApi.Entities.Models;
using DuaTaxi.Services.TaxiApi.Messages.Commands;

using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DuaTaxi.Services.TaxiApi.Services.DriverActivation
{
    public class TaxiDriverActivationService : ITaxiDriverActivationService
    {
        private readonly ITaxiDriverStatusRepository _taxiDriverStatusRepository;
        private readonly IActiveTaxiDriverRepository _activeTaxiDriverRepository;
        private readonly ILogger<TaxiDriverActivationService> _logger;

        public TaxiDriverActivationService(ITaxiDriverStatusRepository taxiDriverRepository, IActiveTaxiDriverRepository activeTaxiDriverRepository, ILogger<TaxiDriverActivationService> logger)
        {
            _taxiDriverStatusRepository = taxiDriverRepository;
            _activeTaxiDriverRepository = activeTaxiDriverRepository;
            _logger = logger;
        }
        public async Task<ActiveDrivers> HandleAsync(ActiveDriverDTO command)
        {
            var driver = await _taxiDriverStatusRepository.GetCustomerAsync(command.CustomerId);

            if (driver is null) {
                _logger.LogError("TaxiDriverActivationHandler --> Driver not exist", null);
                return null;
            }

            if (!driver.Status) {
                _logger.LogError(" TaxiDriverActivationHandler -->Driver status is false", null);

                return null;
            }

            var data = new ActiveDrivers(command.Id, command.CustomerId, command.Latitude, command.Longtitute, true, command.ConnectionId);
            var activeCustomer = await _activeTaxiDriverRepository.GetCustomerAsync(command.Id);
          

            if (activeCustomer is null) {
                await _activeTaxiDriverRepository.AddAsync(data);
            }
            else {
                await _activeTaxiDriverRepository.UpdateAsync(data);
            }


            return data;


        }
    }
}
