﻿using DuaTaxi.Services.TaxiApi.Entities.Models;
using DuaTaxi.Services.TaxiApi.Messages.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DuaTaxi.Services.TaxiApi.Services.DriverActivation
{
    public interface ITaxiDriverActivationService
    {
        Task<ActiveDrivers> HandleAsync(ActiveDriverDTO taxiDriverActivation);
    }
}
