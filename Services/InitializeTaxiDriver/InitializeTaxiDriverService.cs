﻿using DuaTaxi.Common.CustomCheck;
using DuaTaxi.Common.Handlers;
using DuaTaxi.Common.RabbitMq;
using DuaTaxi.Common.RestEase;
using DuaTaxi.Common.WebApiClient;
using DuaTaxi.Repositories.CustomerRepo;
using DuaTaxi.Repositories.TaxiDriverStatusRepo;
using DuaTaxi.Service.TaxiApi.Entities;
using DuaTaxi.Services.TaxiApi.Entities.DTO;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DuaTaxi.Services.TaxiApi.Services.InitializeTaxiDriver
{
    public class InitializeTaxiDriverService : IInitializeTaxiDriverService
    {
        private readonly ICustomerRepository _customerRepository;
        private readonly ITaxiDriverStatusRepository _taxidriverstatusRepository;
        private readonly IBusPublisher _busPublisher;
        private readonly ILogger<InitializeTaxiDriverService> _logger;
        protected IOptions<RestEaseOptions> _RestEaseConfig;
        WebApiClient wac;
        private readonly CheckOptions _check;

        public InitializeTaxiDriverService(
            ITaxiDriverStatusRepository taxidriverstatusRepository,
            IBusPublisher busPublisher,
            IOptions<RestEaseOptions> RestEaseConfig,
            ILogger<InitializeTaxiDriverService> logger,
            ICustomerRepository customerRepository,
            CheckOptions check)
        {
            _customerRepository = customerRepository;
            _taxidriverstatusRepository = taxidriverstatusRepository;
            _busPublisher = busPublisher;
            _logger = logger;
            _RestEaseConfig = RestEaseConfig;
            _check = check;
        }

        public async Task DeleteAllTAxiDriverStatusAsync()
        {
            try {

                await _taxidriverstatusRepository.DeleteAllAsync();

            } catch (Exception ex) {
                _logger.LogError("DeleteAllTAxiDriverStatusAsync exception :", ex.Message);
                throw new Exception();
            }
        }

        public async Task<TaxiDriverStatus> HandleAsync(string CustomerId)
        {
            try {


                var driverExist = await _customerRepository.GetAsync(CustomerId);

                if (driverExist is null) {
                    ////add to TaxiDriverStatus
                    var status = new TaxiDriverStatus(CustomerId, "", false, "Driver not exist");
                    await _taxidriverstatusRepository.AddAsync(status);

                    _logger.LogError("Driver not exist", null);

                    return status;
                }

                // Get From Payment-Service Payment for 
                _logger.LogError($"WebApiClient {_RestEaseConfig} and {_check.Password}", null);
                wac = new WebApiClient(_RestEaseConfig, "payment-service", _check.Password);
                var payment = await wac.LoadAsync<PaymentDto>("Payment/GetByCustomerId", CustomerId);

                if (payment is null) {

                    var status = new TaxiDriverStatus(CustomerId, "", false, "No Payment Exist");
                    await _taxidriverstatusRepository.AddAsync(status);

                    _logger.LogError("No Payment Exist", null);

                    return status;
                }



                if (payment.DayOfExpiration < 0) {

                    var status = new TaxiDriverStatus(CustomerId, "", false, "Payment expired", payment.DayOfExpiration);
                    await _taxidriverstatusRepository.AddAsync(status);


                    _logger.LogError("Payment expired", null);
                    return status;


                }
                else if (payment.DayOfExpiration <= 3.00 && payment.DayOfExpiration > 2.00) {
                    //add to TaxiDriverStatus    
                    var status = new TaxiDriverStatus(CustomerId, "", true, "3 days left", payment.DayOfExpiration);
                    await _taxidriverstatusRepository.AddAsync(status);

                    return status;

                }
                else if (payment.DayOfExpiration < 2.00 && payment.DayOfExpiration > 1.00) {
                    //add to TaxiDriverStatus    
                    var status = new TaxiDriverStatus(CustomerId, "", true, "2 days left", payment.DayOfExpiration);
                    await _taxidriverstatusRepository.AddAsync(status);

                    return status;

                }
                else if (payment.DayOfExpiration < 1) {
                    //add to TaxiDriverStatus    
                    var status = new TaxiDriverStatus(CustomerId, "", true, "1 days left", payment.DayOfExpiration);
                    await _taxidriverstatusRepository.AddAsync(status);

                    return status;

                }
                else {
                    //add to TaxiDriverStatus 

                    var status = new TaxiDriverStatus(CustomerId, "", true, null, payment.DayOfExpiration);
                    await _taxidriverstatusRepository.AddAsync(status);

                    return status;
                }
            } catch (Exception ex) {
                _logger.LogError($"InitializeTaxiDriverService error -----> { ex.Message} {ex.StackTrace} {ex.InnerException} :", ex.Message,ex.StackTrace,ex.InnerException);
                throw new Exception(ex.Message,ex.InnerException);
            }


        }
    }
}
