﻿using DuaTaxi.Service.TaxiApi.Entities;
using DuaTaxi.Services.TaxiApi.Messages.Commands;
using System.Threading.Tasks;

namespace DuaTaxi.Services.TaxiApi.Services.InitializeTaxiDriver
{
    public interface IInitializeTaxiDriverService
    {
        Task<TaxiDriverStatus> HandleAsync(string customerId);

        Task DeleteAllTAxiDriverStatusAsync();
    }
}