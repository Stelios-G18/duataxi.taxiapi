using System;
using System.Threading;
using System.Threading.Tasks;
using DuaTaxi.Services.TaxiApi.Framework;

using Microsoft.AspNetCore.SignalR;

namespace DuaTaxi.Services.TaxiApi.Hubs.Services
{
    public class HubWrapper : IHubWrapper
    {
        private readonly IHubContext<TaxiDriverHub> _hubContext;

        public HubWrapper(IHubContext<TaxiDriverHub> hubContext)
        {
            _hubContext = hubContext;
        }

        public async Task PublishToUserAsync(Guid userId, string message, object data)
            => await _hubContext.Clients.Group(userId.ToUserGroup()).SendAsync(message, data);

        public async Task PublishToAllAsync(string message, object data)
            => await _hubContext.Clients.All.SendAsync(message, data);

        public async Task<string> PublishToUserConnectionIDAsync(string connectionId, string text, object data)
        {
            await _hubContext.Clients.Client(connectionId).SendAsync("confirm", text, data);

            var are = new AutoResetEvent(false);
            string response = null;
            void Callback(string connId, string message)
            {
                if (connectionId == connId) {
                    response = message;
                    are.Set();
                }
            }
            TaxiDriverHub.Response += Callback;
            are.WaitOne(TimeSpan.FromSeconds(15));
            TaxiDriverHub.Response -= Callback;
            return response;

        }

        public  Task SendMessageToUser(string Group, string message)
        {
            return _hubContext.Clients.Group(Guid.Parse(Group).ToUserGroup()).SendAsync("ReceiveMessage", message);
        }
    }
}