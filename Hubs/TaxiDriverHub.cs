using DShop.Common.Authentication;
using DuaTaxi.Services.TaxiApi.Framework;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Threading.Tasks;


namespace DuaTaxi.Services.TaxiApi.Hubs
{
    public class TaxiDriverHub : Hub
    {
        private readonly IJwtHandler _jwtHandler;

        public TaxiDriverHub(IJwtHandler jwtHandler)
        {
            _jwtHandler = jwtHandler;
        }

        public async Task InitializeAsync(string token)
        {
            if (string.IsNullOrWhiteSpace(token))
            {
                await DisconnectAsync();
            }
            try
            {
                //var payload = _jwtHandler.GetTokenPayload(token);
                if (token == null)
                {
                    await DisconnectAsync();
                    
                    return;
                }
                var group = Guid.Parse(token).ToUserGroup();
                await Groups.AddToGroupAsync(Context.ConnectionId, group);
                await ConnectAsync();              
            }
            catch
            {
                await DisconnectAsync();
            }
        }

        private async Task ConnectAsync()
        {
            await Clients.Client(Context.ConnectionId).SendAsync("connected");
          
        }

        private async Task DisconnectAsync()
        {
            await Clients.Client(Context.ConnectionId).SendAsync("disconnected");
        }

        public string GetConnectionId()
        {
            return Context.ConnectionId;
        }

        public static event Action<string, string> Response;
        public void Callback(string connectionId, string message)
        {
            Response?.Invoke(connectionId, message);
        }
        public Task SendMessageToUser(string Group, double message1, double message2)
        {
            return Clients.Group(Guid.Parse(Group).ToUserGroup()).SendAsync("ReceiveMessage", message1, message2);
        }


    }
}